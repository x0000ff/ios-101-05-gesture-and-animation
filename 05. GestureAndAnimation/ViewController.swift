//
//  ViewController.swift
//  05. GestureAndAnimation
//
//  Created by x0000ff on 17/07/15.
//  Copyright (c) 2015 FLC-Team. All rights reserved.
//

//#######################################################################
import UIKit

//#######################################################################
class ViewController: UIViewController {

  //#######################################################################
  @IBOutlet weak var greenView: UIView!

  //#######################################################################
  // Этот метод вызывается (в идеале) один раз
  // когда View нашего ViewController'a было создано
  // Это отличное место для настройки дизайна и дальнейшей инициализции
  //#######################################################################
  override func viewDidLoad() {

    super.viewDidLoad()
    
    // Сделаем greenView круглым
    // КАК? 
    // Установим ему радиус углов равным половине ширины!
    greenView.layer.cornerRadius = greenView.frame.size.width / 2

  }

  //#######################################################################
  @IBAction func tapped(gesture: UITapGestureRecognizer) {
    
    // Определяем позицию нажатия
    let tapPosition : CGPoint = gesture.locationInView(view)

    // Анимация
    UIView.animateWithDuration(0.3, animations: { () -> Void in

      // Меняем центр зеленого View на позицию точки нажатия 
      self.greenView.center = tapPosition
    
    })
    
  }
  //#######################################################################
  
}

